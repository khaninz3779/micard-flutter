import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        home:Scaffold(
          backgroundColor: Colors.lightBlue[900],
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 80,
                    backgroundImage: AssetImage('images/khanin.jpg'),
                  ),
                  Text('Khanin Ch.',
                    style: TextStyle(
                      fontSize: 40,
                      color:Colors.white,
                      fontFamily: 'Kodchasan'
                    )

                  ),
                  Text('Full Stack Developer',
                  style:TextStyle(
                    fontSize:18,
                    color: Colors.white
                  )),
                  SizedBox(
                    height:20,
                    width:150,
                    child: Divider(
                      thickness: 2,
                      color: Colors.white,
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(
                          Icons.phone,
                        color:Colors.lightBlue[900]
                      ),
                      title: Text('+66999999'),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(
                        Icons.mail,
                        color:Colors.lightBlue[900]
                    ),
                      title: Text('Khaninz3779@gmail.com'),
                    ),
                  )
                ],
              ),
            ),
          ),
        )
    );

  }

}
